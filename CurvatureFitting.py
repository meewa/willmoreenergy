import numpy as np
import re
import utils
from paraview import simple, vtk, servermanager

class ROINameNotFound(Exception):
	pass

filler = '#'*42 
spacer = ' '*35

class ExtendedQuadricFitting():
	"""
	Compute principal, mean and gaussian curvatures as describe in the paper:

	author =	"Rao V. Garimella, Blair K. Swartz",
	title =	"Curvature Estimation for Unstructured Triangulations
			 of Surfaces",
	URL =  	"http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.472.9458",
 	"""
	def __init__(self, filename, neighbors=5, ROIname = ""):
		"""
		This class provides the extended quadric fitting method to the mesh using defined number of the "neighbors".
		And as the result mean and gaussian curvatures can be obtained for the consequent calculations.
		"""
		self.fname = filename
		self.neighbors = neighbors if neighbors >=5 else 5
		self.ROIname = ROIname

		self.data = self._getData()

		self.nPoints = self.data.GetNumberOfPoints()
		self.nCells = self.data.GetNumberOfCells()
		self.cellArea = np.zeros((self.nCells,), dtype=np.float64)
		self.cellNormal = np.zeros((self.nCells, 3), dtype=np.float64)
		self.vertexNormal = np.zeros((self.nPoints, 3), dtype=np.float64)
		self.vertexArea = np.zeros((self.nPoints,), dtype=np.float64)

		self.Mean = np.zeros((self.nPoints,), dtype=np.float64)
		self.Gauss = np.zeros((self.nPoints,), dtype=np.float64)

		self.meshSize = 0.		
		self.MaxCellPoints = 0

	def _getData(self):
		reader = simple.OpenDataFile(self.fname)
		data = servermanager.Fetch(reader)

		info = reader.GetDataInformation().DataInformation

		if data.GetDataObjectType() == vtk.VTK_MULTIBLOCK_DATA_SET:
			# Reading cgns-like data
			try:
				for i in xrange(data.GetNumberOfBlocks()):
					for j in xrange(data.GetBlock(i).GetNumberOfBlocks()):

						child = data.GetBlock(i)

						if child.GetMetaData(j).Get(vtk.vtkCompositeDataSet.NAME()) == self.ROIname:

							self.timestep = info.GetTimeSpan()[-1]

							return child.GetBlock(j)

				raise ROINameNotFound(
						filler + " WARNING " + filler + "\n"
						+ spacer + "ROI name no found.\n"
						"Please input the correct name of ROI\n"
						+ filler[:-4] + " END OF WARNING " + filler[:-4] + "\n")
			except ROINameNotFound as err:
				print err

		elif data.GetDataObjectType() == vtk.VTK_POLY_DATA:
			# Reading stl/ply-like data
			return data

	def getVerticesAreas(self):
		if self.vertexArea.any():
			return self.vertexArea
		else:
			return None

	def getData(self):
		if self.data:
			return self.data
		else:
			return None

	def getCellsAreas(self):
		if self.cellArea.any():
			return self.cellArea
		else:
			return None

	def getMeshSize(self):
		if self.meshSize:
			return self.meshSize
		else:
			return None

	def GetCellNormal(self, points):
		vectors = np.array((points.GetPoint(0),
						   points.GetPoint(1),
						   points.GetPoint(2)))
		return utils.getNormal(vectors)

	def GetCellArea(self, points, normal, nPoints):

		self.MaxCellPoints = nPoints if nPoints > self.MaxCellPoints else self.MaxCellPoints

		if nPoints == 3:
			return utils.getTriangleArea(normal)

		elif nPoints > 3:
			# method from the site: http://geomalgorithms.com/a01-_area.html -> area3D_Polygon
			area = 0

			normal = utils.normalizeVector(normal)
			# select largest abs coordinate to ignore for projection
			ax, ay, az = np.abs(normal)

			coord = 3                 # ignore z-coord
			if ax > ay and ax > az:
				coord = 1			  # ignore x-coord
			elif ay > az:
				coord = 2			  # ignore y-coord

			# compute area of the 2D projection
			
			for i, j, k in zip(xrange(1, nPoints), xrange(2, nPoints), xrange(nPoints)):
				if coord == 1:
					area += points.GetPoint(i)[1] * \
							(points.GetPoint(j)[2] - points.GetPoint(k)[2])
				elif coord == 2:
					area += points.GetPoint(i)[2] * \
							(points.GetPoint(j)[0] - points.GetPoint(k)[0])
				elif coord == 3:
					area += points.GetPoint(i)[0] * \
							(points.GetPoint(j)[1] - points.GetPoint(k)[1])

			# wrap-around term
			if coord == 1:
				area += points.GetPoint(0)[1] * \
						(points.GetPoint(1)[2] - points.GetPoint(nPoints-1)[2])
			elif coord == 2:
				area += points.GetPoint(0)[2] * \
						(points.GetPoint(1)[0] - points.GetPoint(nPoints-1)[0])
			elif coord == 3:
				area += points.GetPoint(0)[0] * \
						(points.GetPoint(1)[1] - points.GetPoint(nPoints-1)[1])

			# scale to get area before projection
			an = np.sqrt(np.einsum('...i,...i', normal, normal)) # length of normal vector

			area *= (an / (2 * normal[coord - 1]))

			return area

	def getValuesFromVertex(self, ptId):
		# ptId - index of point in dataset

		cells = vtk.vtkIdList()
		self.data.GetPointCells(ptId, cells)

		firstCircleIds = set()
		prevCellId = -1 # coz data from cgns file have duplicated ids of cells
		for i in xrange(cells.GetNumberOfIds()):

			cellId = cells.GetId(i)
			if cellId != prevCellId:
				prevCellId = cellId
				cell = self.data.GetCell(cellId)

				points = cell.GetPoints()
				nPoints = points.GetNumberOfPoints()

				# coz cgns Cells have bound point and repeat the last point
				if points.GetPoint(nPoints-1) == points.GetPoint(nPoints-2):
					nPoints -= 1
		
				if nPoints > 2:
					
					firstCircleIds |= self.vtkIdListToSet(cell.GetPointIds())

					if not self.cellArea[cellId] and not self.cellNormal[cellId].all():
						normal = self.GetCellNormal(points)
						self.cellArea[cellId] = self.GetCellArea(points, normal, nPoints)
						self.cellNormal[cellId] = utils.normalizeVector(normal)

					self.vertexArea[ptId] += self.cellArea[cellId] / nPoints
					self.vertexNormal[ptId] += self.cellNormal[cellId]*self.cellArea[cellId]

		self.vertexNormal[ptId] = utils.normalizeVector(self.vertexNormal[ptId])

		firstCircleIds.remove(ptId)
		return firstCircleIds

	def vtkIdListToSet(self, vtkIdList):
		'''Takes vtkIdsList type and returns unique numpy array of ids'''
		nIds = vtkIdList.GetNumberOfIds()
		arr = set()
		for i in xrange(nIds):
			arr.add(vtkIdList.GetId(i))
		return arr

	def _getRotationMatrix(self, normal):
		r3 = normal

		if r3.tolist() not in [[1,0,0], [-1,0,0]]:
			i = np.array([1, 0, 0])
		else:
			i = np.array([0, 1, 0])	
					
		r1 = (np.identity(3) - np.outer(r3, r3)).dot(i)
		r1 = utils.normalizeVector(r1)

		r2 = np.array(utils.cross(r3, r1))

		return np.array([r1,r2,r3])

	def _getSecondLevelPoints(self, ptId, firstCircleIds):
		new_points = set()
		prevCellId = -1 # coz data from cgns have duplicated ids of cells
		for p in firstCircleIds:
			cells = vtk.vtkIdList()
			self.data.GetPointCells(p, cells)

			for i in xrange(cells.GetNumberOfIds()):

				cellId = cells.GetId(i)
				if cellId != prevCellId:
					prevCellId = cellId
					cell = self.data.GetCell(cellId)
					new_points |= self.vtkIdListToSet(cell.GetPointIds())

		new_points.remove(ptId)
		return list(new_points - firstCircleIds)

	def _getSetOfPoints(self, ptId, firstCircleIds, N = 5):
		'''
		Chooses the set of neighboring points to a current vertex
		'''

		pointsIds = list(firstCircleIds)

		if len(pointsIds) < N:			
			new_points = self._getSecondLevelPoints(ptId, firstCircleIds)
			dif = N - len(pointsIds)
			if dif < len(new_points):
				new_points = np.random.choice(new_points, dif, replace=False)
				
			X = list(firstCircleIds | set(new_points))

		elif len(pointsIds) > N:
			X = np.random.choice(pointsIds, N, replace=False)

		else:
			X = pointsIds

		return np.array(X)

	def calculateCurvatures(self, neighbor = 5, repeat=1):
		'''
		Calculates mean and gaussian curvatures

		#########

		Input:
		neighbor - number of points neighboring to a current vertex 
				   which are considered for the calculation
		repeat - determines how many times the curvature values computed 
				 for a current vertex and then returns the averaged value
		'''
		N = neighbor if neighbor>=5 else 5

		for ptId in xrange(self.nPoints):
			firstCircleIds = self.getValuesFromVertex(ptId)
			R = self._getRotationMatrix(self.vertexNormal[ptId])
	
			mean = 0
			gauss = 0
			point = self.data.GetPoint(ptId)

			for j in xrange(repeat):
				
				X = self._getSetOfPoints(ptId, firstCircleIds, N)

				new_X = np.array([R.dot(self.data.GetPoint(x) - np.array(point)) for x in X])

				A = np.array([[v[0]**2, v[0]*v[1], v[1]**2, v[0], v[1]] for v in new_X])

				# if N==5:
				# 	Coeff = np.linalg.solve(A, new_X[:,2])
				# else:
				Coeff = np.linalg.lstsq(A, new_X[:,2], rcond=None)[0]

				mean += self._meanCurvature(Coeff)/repeat
				gauss += self._gaussianCurvature(Coeff)/repeat

			self.Mean[ptId] = mean
			self.Gauss[ptId] = gauss

		self.meshSize = np.max(self.cellArea)

	def _meanCurvature(self, coeff):
		'''
		Calculates mean curvature according to the article
		'''
		a, b, c, d, e = coeff

		return (a + c + a*e**2 + c*d**2 - b*d*e)/(1 + d**2 + e**2)**1.5 

	def _gaussianCurvature(self, coeff):
		'''
		Calculates gaussian curvature according to the article
		'''
		a, b, c, d, e = coeff
		
		return (4*a*c - b**2)/(1 + d**2 + e**2)**2

	def MeanCurvature(self):
		return self.Mean

	def GaussianCurvature(self):
		return self.Gauss

	def meanMeanCurvature(self):
		'''
		Returns the area-averaged mean curvature
		'''
		if self.Mean.any():
			return np.sum(self.Mean*self.vertexArea)/np.sum(self.vertexArea)
		else:
			return None

	def meanGaussianCurvature(self):
		'''
		Return the area-averaged gaussian curvature
		'''
		if self.Gauss.any():
			return np.sum(self.Gauss*self.vertexArea)/np.sum(self.vertexArea)
		else:
			return None

	def GetVericeCoord(self):
		vertices = np.zeros((self.nPoints,3), dtype=np.float64)
		for i in xrange(self.nPoints):
			vertices[i] = np.array(self.data.GetPoint(i), dtype=np.float64)
		return vertices

	def GetCellsIds(self):
		faceindices = np.zeros((self.nCells,3), dtype=np.uint16)
		for i in xrange(self.nCells):
			cell = self.data.GetCell(i)
			faceindices[i] = np.array(list(self.vtkIdListToSet(cell.GetPointIds())),
								    dtype=np.uint16)
		return faceindices

	def isTriangleMesh(self):
		return self.MaxCellPoints == 3

	def saveToPLY(self, fname, text = False, curvType = "Mean"):
		#define only for the triangle meshes

		if self.Mean.any() and self.isTriangleMesh():
			vertices = self.GetVericeCoord()
			vertices_normal = np.hstack((vertices, self.vertexNormal))
			if curvType == "Mean":
				curv = self.Mean
			elif curvType == "Gaussian":
				curv = self.Gauss
			vert_quality = np.insert(vertices_normal, 6, curv, axis=1)
			
			vertex = np.array(list(map(tuple, vert_quality)), 
					   dtype=[('x', 'f4'), ('y', 'f4'),
	                          ('z', 'f4'), ('nx', 'f4'), 
	                          ('ny', 'f4'), ('nz', 'f4'), 
	                          ('quality', 'f4')])

			face = [(cell,) for cell in self.GetCellsIds()]

			face = np.array(face,
	                    dtype=[('vertex_indices', 'i4', (3,))])

			from plyfile import PlyData, PlyElement

			elVertex = PlyElement.describe(vertex, "vertex")
			elFace = PlyElement.describe(face.ravel(), "face")

			if not re.search(r"\.\w+", fname):
				fname += ".ply"
			else:
				fname = re.sub(r"\.\w+", ".ply", fname)

			PlyData([elVertex,elFace], text=text).write(fname)
		else:
			print "This method is not defined for the mesh with dimension equal ", self.MaxCellPoints

if __name__ == "__main__":

	import cProfile, pstats, StringIO
	import os, timeit
	import timeit
	start_time = timeit.default_timer()

	objectpath = r'E:\Users\Mikhail\Documents\Physics\IGIL\Work\Curvatures\Ansys\Bulge_files\dp0\CFX-1\CFX\Fluid Flow CFX_001'
	fname = os.path.join(objectpath, r'ExportResults1_mesh.cgns')
	fit = ExtendedQuadricFitting(fname, ROIname = "Aneurysma")
	print "finish read", timeit.default_timer() - start_time

	#fit.saveToPLY(fname, text = True)
	############################################
	# Time management 

	pr = cProfile.Profile()
	pr.enable()  # start profiling

	fit.calculateCurvatures()
	# It's a chunk of the code which i wanna check
	#fit.calculateCurvatures(8, 5)
	#import willmore as wm
	#print(wm.willmore(fit))
	pr.disable()  # end profiling
	s = StringIO.StringIO()
	sortby = 'tottime'
	ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
	ps.print_stats()
	print s.getvalue()
	
	#############################################
	# Saving the file into ply format

	# fit.saveToPLY(os.path.join(objectpath, r'Sphere\testSphere'))
	print "areaVert", np.sum(fit.getVerticesAreas())
	print "areaCell", np.sum(fit.getCellsAreas())
	print "Mean", fit.meanMeanCurvature()
	print "Gauss", fit.meanGaussianCurvature()
	print "meshSize", fit.getMeshSize()