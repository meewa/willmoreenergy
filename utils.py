import numpy as np

def normalizeVector(vector):
    vector /= np.sqrt(np.einsum('...i,...i', vector, vector))

    return vector

def cross(x, y):
    """
    This simple way is working faster than np.cross :P
    """
    return np.array([x[1]*y[2] - x[2]*y[1],
            x[2]*y[0] - x[0]*y[2],
            x[0]*y[1] - x[1]*y[0]])

def getNormal(vectors):
    return np.array(cross(vectors[1] - vectors[0],
                 vectors[2] - vectors[0]))

def getTriangleArea(normal):
    return .5*np.sqrt((normal**2).sum())

def addToArray(adding, adder):
    return np.concatenate((adding, adder))
