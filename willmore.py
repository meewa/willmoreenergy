import os
import glob
import numpy as np
import utils

def incidentFacesDict(vertices, faces):
	"""
	Creates a dictionary where the each vertex contains the list of surface area
	of faces which are incident to vertex
	"""

	vertDict = {i:{'area':[], 'curvature':[]} for i in range(vertices.count)}

	for face in faces["vertex_indices"]:
		coord = verticesCoordOfFace(face, vertices)
		area = faceArea(coord)/len(face)

		for i in face:
			vertDict[i]['area'].append(area)

	return vertDict

def verticesCoordOfFace(face, vertices):
	return [[vertices['x'][vert], vertices['y'][vert], vertices['z'][vert]] 
			 for vert in face]

def willmorePLY(meandata):
	nVert = meandata["vertex"].count
	integral_mean = 0 

	faceDict = incidentFacesDict(meandata["vertex"], meandata["face"])

	for vert in range(nVert):
		area = np.sum(faceDict[vert]['area'])
		integral_mean += (meandata["vertex"]["quality"][vert] ** 2) * area

	return integral_mean

def willmore(curvatureFitData):
	meandata = curvatureFitData.MeanCurvature()
	if meandata.Mean.any():
			
		areas = curvatureFitData.getVerticesAreas()

		return np.sum(meandata**2 * areas)
	else:
		return None
